create database QLSV;

CREATE TABLE QLSV.DMKHOA (MaKH varchar(6), TenKhoa varchar(30));

alter table QLSV.DMKHOA add CONSTRAINT DMKHOA_PK PRIMARY KEY(MaKH);

CREATE TABLE QLSV.SINHVIEN (MaSV varchar(6), HoSV varchar(30), TenSV varchar(15), GioiTinh char(1), NgaySinh DateTime, NoiSinh varchar(50), DiaChi varchar(50), MaKH varchar(6), HocBong int);

alter table QLSV.SINHVIEN add CONSTRAINT SINHVIEN_PK PRIMARY KEY(MaSV);